//
//  Cube.h
//  RubiksCube3D
//
//  Created by Stefan Seritan on 10/20/11.

#ifndef RubiksCube3D_Cube_h
#define RubiksCube3D_Cube_h

#include <vector>
#include "Cubelet.h"

class Cube{
public:
    Cube();
    ~Cube();
    void drawCube(int dimension);
    void updateRotation(int axis, int direction);
    void selectFace(int faceCode);
    void deperspectifyFaceCode(int axis, int &faceCode);
    
private:
    float xRotationAngle; float yRotationAngle; float zRotationAngle;
    //Vector to hold all Cubelets in 2x2
    std::vector<Cubelet *> Cubelets2x2;
    //Holds the currently selected face (starts with 4 cubelets in them (in the order of initialization) UNSELECTED)
    std::vector<Cubelet *> selectedFace;
};

#endif
