//
//  Cubelet.h
//  RubiksCube3D
//
//  Created by Stefan Seritan on 10/19/11.

#ifndef RubiksCube3D_Cubelet_h
#define RubiksCube3D_Cubelet_h

#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#include <vector>

class Cubelet{
public:
    Cubelet(int fC, int bC, int uC, int dC, int rC, int lC, float x, float y, float z);
    ~Cubelet();
    void drawCubelet();
    void setSelectionStatus(int status);
    bool faceStatus(int faceCode);
    
private:
    GLuint loadTexture(const char * const fileName);
    void setFaceTexture(int color);        
    float compareCoordinates(float coord1, float coord2);

    //Face colors
    int fColor; int bColor; int uColor; int dColor; int rColor; int lColor;
    //Selection status
    int selectionStatus;
    //Position
    float cubeletX; float cubeletY; float cubeletZ;
    //Angles in degrees
    float xLocalAngle; float yLocalAngle; float zLocalAngle;
    //Textures
    GLuint regWhiteTexture; GLuint regGreenTexture; GLuint regRedTexture; GLuint blackTexture;
    GLuint regPurpleTexture; GLuint regBlueTexture; GLuint regYellowTexture;
    GLuint selWhiteTexture; GLuint selGreenTexture; GLuint selRedTexture;
    GLuint selPurpleTexture; GLuint selBlueTexture; GLuint selYellowTexture;
};  

#endif
