//
//  PNGToTexture.h
//  RubiksCube3D
//
//  Created by Stefan Seritan on 11/14/11.

#ifndef RubiksCube3D_PNGToTexture_h
#define RubiksCube3D_PNGToTexture_h

#include <GLUT/GLUT.h>
#include <png.h>

class PNGToTexture {
public:
    bool loadPNGImage(const char * const fileName, int &outWidth, int &outHeight,
                      bool &outHasAlpha, GLubyte *&outData);
};


#endif
