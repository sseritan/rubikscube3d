//
//  Cubelet.cpp
//  RubiksCube3D
//
//  Created by Stefan Seritan on 10/19/11.

#include <iostream>
#include <fstream>
#include <cmath>
#include "constants.h"
#include "Cubelet.h"
#include "PNGToTexture.h"

using namespace std;


//Constructor And Destructor
Cubelet::Cubelet(int fC, int bC, int uC, int dC, int rC, int lC, float x, float y, float z) {
    fColor = fC; bColor = bC; uColor = uC; dColor = dC; rColor = rC; lColor = lC;
    selectionStatus = UNSELECTED;
    cubeletX = x; cubeletY = y; cubeletZ = z; xLocalAngle = 0.0; yLocalAngle = 0.0; zLocalAngle = 0.0;
    //TODO: Get local path somehow
    regWhiteTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegWhiteSquare.png");
    blackTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegBlackSquare.png");
    regRedTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegRedSquare.png");
    regBlueTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegBlueSquare.png");
    regGreenTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegGreenSquare.png");
    regYellowTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegYellowSquare.png");
    regPurpleTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/RegPurpleSquare.png");
    selWhiteTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/SelWhiteSquare.png");
    selRedTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/SelRedSquare.png");
    selBlueTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/SelBlueSquare.png");
    selGreenTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/SelGreenSquare.png");
    selYellowTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/SelYellowSquare.png");
    selPurpleTexture = loadTexture("/Users/stefan/Documents/Projects/Programming/RubiksCube/RubiksCube3D/Pictures/SelPurpleSquare.png");
}

Cubelet::~Cubelet() {
    glDeleteTextures(1, &blackTexture);
    glDeleteTextures(1, &regWhiteTexture); glDeleteTextures(1, &regRedTexture); glDeleteTextures(1, &regBlueTexture);
    glDeleteTextures(1, &regPurpleTexture); glDeleteTextures(1, &regYellowTexture); glDeleteTextures(1, &regGreenTexture);
    glDeleteTextures(1, &selWhiteTexture); glDeleteTextures(1, &selRedTexture); glDeleteTextures(1, &selBlueTexture);
    glDeleteTextures(1, &selPurpleTexture); glDeleteTextures(1, &selYellowTexture); glDeleteTextures(1, &selGreenTexture);
}

//Function to load textures from PNGs
GLuint Cubelet::loadTexture(const char *const fileName) {
    GLuint texture;
    GLubyte *textureData;
    int imageWidth, imageHeight;
    bool hasAlpha;
    PNGToTexture * png = new PNGToTexture();
    
    bool successfulImageLoad = png->loadPNGImage(fileName, imageWidth, imageHeight, hasAlpha, textureData);
    if (!successfulImageLoad) {
        cout << "Problem loading PNG file" << endl;
        return texture;
    }
    
    //Create and bind the texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    
    //Sets unpacking alignment (to byte alignment)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    //Generate the texture from our image data
    glTexImage2D(GL_TEXTURE_2D, 0, hasAlpha ? 4:3, imageWidth, imageHeight, 0, hasAlpha ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, textureData);
    
    //Many parameters to set. Texture wrap/clamp stops the image from wrapping around if it exceeds its boundaries,
    //filters set quality at close and far range (gl_linear is best for non mipmap formats)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    
    //Memory Management
    delete textureData;
    
    return texture;
}

//Drawing functions
void Cubelet::drawCubelet() {
    glPushMatrix();
    glTranslatef(cubeletX, cubeletY, cubeletZ);
    glRotatef(xLocalAngle, 1.0, 0.0, 0.0);
    glRotatef(yLocalAngle, 0.0, 1.0, 0.0);
    glRotatef(zLocalAngle, 0.0, 0.0, 1.0);
    
    //Front Square (White)
    glEnable(GL_TEXTURE_2D);
    setFaceTexture(fColor);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, 1.0, 1.0);
        glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2f(0.0, 1.0); glVertex3f(1.0, -1.0, 1.0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    //Back Square (Yellow)
    glEnable(GL_TEXTURE_2D);
    setFaceTexture(bColor);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(0.0, 1.0); glVertex3f(1.0, -1.0, -1.0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    //Up Square (Blue)
    glEnable(GL_TEXTURE_2D);
    setFaceTexture(uColor);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, 1.0, 1.0);
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2f(0.0, 1.0); glVertex3f(1.0, 1.0, 1.0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    
    //Down Square (Green)
    glEnable(GL_TEXTURE_2D);
    setFaceTexture(dColor);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(1.0, 1.0); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2f(0.0, 1.0); glVertex3f(1.0, -1.0, 1.0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    //Right Square (Red)
    glEnable(GL_TEXTURE_2D);
    setFaceTexture(rColor);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2f(1.0, 0.0); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2f(0.0, 1.0); glVertex3f(1.0, 1.0, 1.0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    //Left Square (Purple)
    glEnable(GL_TEXTURE_2D);
    setFaceTexture(lColor);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, 1.0, 1.0);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
    glPopMatrix();
}

//Sets the correct texture
void Cubelet::setFaceTexture(int color) {
    switch (color) {
        case WHITE:
            if (selectionStatus == UNSELECTED) {
                glBindTexture(GL_TEXTURE_2D, regWhiteTexture);
            } else if (selectionStatus == SELECTED) {
                glBindTexture(GL_TEXTURE_2D, selWhiteTexture);
            }
            break;
        case YELLOW:
            if (selectionStatus == UNSELECTED) {
                glBindTexture(GL_TEXTURE_2D, regYellowTexture);
            } else if (selectionStatus == SELECTED) {
                glBindTexture(GL_TEXTURE_2D, selYellowTexture);
            }
            break;
        case BLUE:
            if (selectionStatus == UNSELECTED) {
                glBindTexture(GL_TEXTURE_2D, regBlueTexture);
            } else if (selectionStatus == SELECTED) {
                glBindTexture(GL_TEXTURE_2D, selBlueTexture);
            }
            break;
        case GREEN:
            if (selectionStatus == UNSELECTED) {
                glBindTexture(GL_TEXTURE_2D, regGreenTexture);
            } else if (selectionStatus == SELECTED) {
                glBindTexture(GL_TEXTURE_2D, selGreenTexture);
            }
            break;
        case RED:
            if (selectionStatus == UNSELECTED) {
                glBindTexture(GL_TEXTURE_2D, regRedTexture);
            } else if (selectionStatus == SELECTED) {
                glBindTexture(GL_TEXTURE_2D, selRedTexture);
            }
            break;
        case PURPLE:
            if (selectionStatus == UNSELECTED) {
                glBindTexture(GL_TEXTURE_2D, regPurpleTexture);
            } else if (selectionStatus == SELECTED) {
                glBindTexture(GL_TEXTURE_2D, selPurpleTexture);
            }
            break;
        case BLACK:
            glBindTexture(GL_TEXTURE_2D, blackTexture);
            break;
    }
}

//Setter for selection status
void Cubelet::setSelectionStatus(int status) {
    selectionStatus = status;
    drawCubelet();
    glutPostRedisplay();
}

//Tells the cube whether it is in a deperspectified face
bool Cubelet::faceStatus(int faceCode) {
    //Return true only if it is in the face
    //Is in F if z = 1, B if z = -1, R if x = 1, L if x =-1, U if y = 1, and D if y = -1
    switch (faceCode) {
        case F:
            if (cubeletZ == 1) return true;
            break;
        case B:
            if (cubeletZ == -1) return true;
            break;
        case R:
            if (cubeletX == 1) return true;
            break;
        case L:
            if (cubeletX == -1) return true;
            break;
        case U:
            if (cubeletY == 1) return true;
            break;
        case D:
            if (cubeletY == -1) return true;
            break;
    }
    
    return false;
}