//
//  main.h
//  RubiksCube3D
//
//  Created by Stefan Seritan on 10/19/11.

#ifndef RubiksCube3D_main_h
#define RubiksCube3D_main_h

#include "Cube.h"

//Function Prototypes
void handleKeyPress(unsigned char key, int x, int y);
void handleSpecialKeyPress(int key, int x, int y);
void keyActions();
void initRendering();
void handleResize(int w, int h);
void drawScene();

int DIMENSION = 2;
Cube * cube;
#endif
