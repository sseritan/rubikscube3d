//
//  Cube.cpp
//  RubiksCube3D
//
//  Created by Stefan Seritan on 10/20/11.

#include <iostream>
#include <cmath>
#include "constants.h"
#include "Cubelet.h"
#include "Cube.h"

using namespace std;

//Constructors and Destrutors (initialize all the cubelets and data I need)
Cube::Cube(){
    xRotationAngle = 0.0; yRotationAngle = 0.0; zRotationAngle = 0.0;
    
    //Array that holds the starting positions and colors for the anonymous 2x2 cubelets
    int initialData2x2 [72] = {
        WHITE, BLACK, BLUE, BLACK, RED, BLACK, 1, 1, 1,
        WHITE, BLACK, BLUE, BLACK, BLACK, PURPLE, -1, 1, 1,
        WHITE, BLACK, BLACK, GREEN, RED, BLACK, 1.0, -1.0, 1.0,
        WHITE, BLACK, BLACK, GREEN, BLACK, PURPLE, -1.0, -1.0, 1.0,
        BLACK, YELLOW, BLUE, BLACK, RED, BLACK, 1.0, 1.0, -1.0,
        BLACK, YELLOW, BLUE, BLACK, BLACK, PURPLE, -1.0, 1.0, -1.0,
        BLACK, YELLOW, BLACK, GREEN, RED, BLACK, 1.0, -1.0, -1.0,
        BLACK, YELLOW, BLACK, GREEN, BLACK, PURPLE, -1.0, -1.0, -1.0
    };
    //Initializes all anonymous 2x2 cubelets into a vector to hold them
    for (int i = 0; i < 8; i++) {
        Cubelets2x2.push_back(new Cubelet(initialData2x2[9*i],initialData2x2[9*i+1],initialData2x2[9*i+2],
                                          initialData2x2[9*i+3],initialData2x2[9*i+4],initialData2x2[9*i+5],
                                          (float)initialData2x2[9*i+6],(float)initialData2x2[9*i+7],(float)initialData2x2[9*i+8]));
    }
    //Place 4 cubelets in the selected face so we can generalize later
    for (int i = 0; i < 4; i++) {
        selectedFace.insert(selectedFace.begin(), Cubelets2x2[i]);
    }
}

Cube::~Cube(){
    for (int i = 0; i < 8; i++) {
        delete Cubelets2x2[i];
    }
}

//For reference: Order of faces goes front, back, up, down, right, left
void Cube::drawCube(int dimension) {
    glRotatef(xRotationAngle, 1.0, 0.0, 0.0);
    glRotatef(yRotationAngle, 0.0, 1.0, 0.0);
    glRotatef(zRotationAngle, 0.0, 0.0, 1.0);
    switch(dimension) {
        case 2:
            for (int i = 0; i < 8; i++) {
                Cubelets2x2[i]->drawCubelet();
            }
            break;
    }
}

//Rotation functions for the whole cube
//TODO:Animate these
void Cube::updateRotation(int axis, int direction) {
    switch (axis) {
        case X_AXIS:
            //Increment the cube's rotation
            xRotationAngle += direction*30;
            //Keep the rotation between 0 and 360
            if (xRotationAngle >= 360) {
                xRotationAngle -=360;
            } else if (xRotationAngle <= 0) {
                xRotationAngle += 360;
            }
            break;
        case Y_AXIS:
            yRotationAngle += direction*30;
            if (yRotationAngle >= 360) {
                yRotationAngle -=360;
            } else if (yRotationAngle <= 0) {
                yRotationAngle += 360;
            }
            break;
        case Z_AXIS:
            zRotationAngle += direction*30;
            if (zRotationAngle >= 360) {
                zRotationAngle -=360;
            } else if (zRotationAngle <= 0) {
                zRotationAngle += 360;
            }
            break;
    }
}

//Face selection method (currently only tested on 2 by 2)
void Cube::selectFace(int faceCode) {
    //First undo the cube's rotations by "perspectifying" the face code
    deperspectifyFaceCode(X_AXIS, faceCode);
    deperspectifyFaceCode(Y_AXIS, faceCode);
    deperspectifyFaceCode(Z_AXIS, faceCode);
    
    int count = 0;
    //Get all the cubelets in that face and select htem
    for (int i = 0; i < 8; i++){
        if (Cubelets2x2[i]->faceStatus(faceCode)) {
            //Unselect and delete whatever is in that slot
            selectedFace[count]->setSelectionStatus(UNSELECTED);
            selectedFace.erase(selectedFace.begin()+count);
            //Select and insert into free slot
            selectedFace.insert(selectedFace.begin()+count,Cubelets2x2[i]);
            selectedFace[count]->setSelectionStatus(SELECTED);
            count++;
        }
    }
}

//Helper function to always select correct face when the user has rotated the cube
void Cube::deperspectifyFaceCode(int axis, int &faceCode) {
    int count; int corr [4]; float angle;
    
    //Depending on which axis, the faceCode that can be changed are different
    switch (axis) {
        case X_AXIS:
            if (faceCode == U || faceCode == D) {
                return;
            }
            corr[0] = F; corr[1] = R; corr[2] = B; corr[3] = L;
            angle = xRotationAngle;
            break;
        case Y_AXIS:
            if (faceCode == R || faceCode == L) {
                return;
            }
            corr[0] = F; corr[1] = U; corr[2] = B; corr[3] = D;
            angle = yRotationAngle;
            break;
        case Z_AXIS:
            if (faceCode == F || faceCode == B) {
                return;
            }
            corr[0] = U; corr[1] = R; corr[2] = D; corr[3] = L;
            angle = zRotationAngle;
            break;
    }
    
    //Find where our face code is in the array
    for (int i = 0; i < 4; i++) {
        if (faceCode == corr[i]) {
            count = i;
        }
    }
    
    //We have four "quadrants". If the rotation is 330, 0, 30, we do nothing
    //If it is 60, 90, 120, we want to move the faceCode left one through the array
    //If it is 150, 180, 210, we want to move the faceCode left 2 through the array
    //If it is 240, 270,300, we want to move the faceCode left 3 through the array
    if (fabs(angle - 90) <= 30) {
        count -= 1;
    } else if (fabs(angle - 180) <= 30) {
        count -= 2;
    } else if (fabs(angle - 270) <= 30) {
        count -= 3;
    }
    
    //"Wrap" count to still be in the correction array.
    //We only check the negative case because we never add to count, only subtract
    if (count < 0) {
        count += 4;
    }
    
    faceCode = corr[count];
}
