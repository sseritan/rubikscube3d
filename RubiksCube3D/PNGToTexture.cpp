//
//  PNGToTexture.cpp
//  RubiksCube3D
//
//  Created by Stefan Seritan on 11/14/11.
//
//  Based on code from
//  http://blog.nobel-joergensen.com/2010/11/07/loading-a-png-as-texture-in-opengl-using-libpng/

#include <iostream>
#include <cstdio>
#include <png.h>
#include "PNGToTexture.h"

using namespace std;

bool PNGToTexture::loadPNGImage(const char *const fileName, int &outWidth, int &outHeight,
                                bool &outHasAlpha, GLubyte * &outData) {
    png_structp PNGPointer;
    png_infop PNGDataPointer;
    unsigned int signaturePos = 0;
    png_uint_32 width, height;
    int bitDepth, colorType, interlaceType;
    FILE * PNGFile;
    
    if ((PNGFile = fopen(fileName, "rb")) == NULL) {
        cout << "Problem opening file " << fileName << endl;
        return false;
    }
    
    //Initialize png_struct with error handling
    //NULL is default error methods
    PNGPointer = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (PNGPointer == NULL) {
        cout << "Problem initializing PNGPointer" << endl;
        fclose(PNGFile);
        return false;
    }
    
    //Allocate and init memory for image info
    PNGDataPointer = png_create_info_struct(PNGPointer);
    if (PNGDataPointer == NULL) {
        fclose(PNGFile);
        png_destroy_read_struct(&PNGPointer, NULL, NULL);
        return false;
    }
    
    //Set more error handling
    if (setjmp(png_jmpbuf(PNGPointer))) {
        png_destroy_read_struct(&PNGPointer, &PNGDataPointer, NULL);
        fclose(PNGFile);
        return false;
    }
    
    //Output control
    png_init_io(PNGPointer, PNGFile);
    
    //If we already have some of the signature
    png_set_sig_bytes(PNGPointer, signaturePos);
    
    //Read image data in from the file (forcing 8 bit)
    png_read_png(PNGPointer, PNGDataPointer, PNG_TRANSFORM_PACKING, NULL);
    
    //Get info from png_infop struct
    png_get_IHDR(PNGPointer, PNGDataPointer, &width, &height, &bitDepth, &colorType, &interlaceType, NULL, NULL);
    outWidth = width;
    outHeight = height;
    switch (colorType) {
        case PNG_COLOR_TYPE_RGBA:
            outHasAlpha = true;
            break;
        case PNG_COLOR_TYPE_RGB:
            outHasAlpha = false;
            break;
        default:
            cout << "Color type " << colorType << " not supported." << endl;
            png_destroy_read_struct(&PNGPointer, &PNGDataPointer, NULL);
            fclose(PNGFile);
            return false;
    }
    
    unsigned int rowBytes = (unsigned int)png_get_rowbytes(PNGPointer, PNGDataPointer);
    outData = new unsigned char [rowBytes * outHeight];
    
    png_bytepp rowPointers = png_get_rows(PNGPointer, PNGDataPointer);
    //Swap rows for OpenGL (which reads bottom to top)
    for (int i = 0; i < outHeight; i++) {
        memcpy(outData+(rowBytes * (outHeight-1-i)), rowPointers[i], rowBytes);
    }
    
    //Free memory
    png_destroy_read_struct(&PNGPointer, &PNGDataPointer, NULL);
    fclose(PNGFile);
    
    return true;
}