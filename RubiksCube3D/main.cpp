//
//  main.cpp
//  RubiksCube3D
//
//  Created by Stefan Seritan on 10/9/11.

#include <iostream>
#include <stdlib.h>
//Necessary libraries for OpenGL
#include <OpenGl/OpenGL.h>
#include <GLUT/GLUT.h>

#include "constants.h"
#include "main.h"
#include "Cubelet.h"
#include "Cube.h"

using namespace std;

int main (int argc, char** argv)
{
    //Init GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(600, 600);
    
    //Create windows
    glutCreateWindow("Rubik's Cube 3D");
    initRendering();
    
    //Set handlers
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeyPress);
    glutSpecialFunc(handleSpecialKeyPress);
    glutReshapeFunc(handleResize);
    
    //Initialize cube object
    cube = new Cube;
    
    glutMainLoop();
    
    //Memory Management
    delete cube;
    
    return 0;
}

//The major part of my code. Draws the 3D scene
void drawScene(){
    //Clears what was previously onscreen, and set background color
    glClearColor(0.40f, 0.70f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    //Set the cube's position farther back
    glTranslatef(0.0, 0.0, -12.0);
    
    //Draw the cube
    cube->drawCube(DIMENSION);
    
    //Send the 3D scene to be printed onscreen
    glutSwapBuffers();
}

//Initialize 3D rendering
void initRendering(){
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
}

//Handles keyboard presses (which will be greatly needed later on).
//Takes which key is pressed, and the coordinates of the cursor.
void handleKeyPress(unsigned char key, int x, int y) {
    switch (key) {
        case 27:
            exit(0);
            break;
        case 'z':
            cube->updateRotation(Z_AXIS, CLOCKWISE);
            break;
        case 'Z':
            cube->updateRotation(Z_AXIS, COUNTERCLOCKWISE);
            break;
        case 's':
            cube->selectFace(F);
            break;
        case 'S':
            cube->selectFace(B);
            break;
        case 'a':
            break;
        case '2':
            DIMENSION = 2;
            break;
    }
    
    glutPostRedisplay();
}

//Handle special key presses
void handleSpecialKeyPress(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_DOWN:
            cube->updateRotation(X_AXIS, COUNTERCLOCKWISE);
            break;
        case GLUT_KEY_RIGHT:
            cube->updateRotation(Y_AXIS, COUNTERCLOCKWISE);
            break;
        case GLUT_KEY_UP:
            cube->updateRotation(X_AXIS, CLOCKWISE);
            break;
        case GLUT_KEY_LEFT:
            cube->updateRotation(Y_AXIS, CLOCKWISE);
            break;
    }
    
    glutPostRedisplay();
}

//Handles window resizing
void handleResize(int w, int h) {
    //Main cube viewport
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)w/(double)h, 1.0, 200.0);
}

