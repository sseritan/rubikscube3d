//
//  constants.h
//  RubiksCube3D
//
//  Created by Stefan Seritan on 12/23/11.
//

#ifndef RubiksCube3D_constants_h
#define RubiksCube3D_constants_h

enum {CLOCKWISE = -1, COUNTERCLOCKWISE = 1};
enum {X_AXIS = 2, Y_AXIS, Z_AXIS};
enum {UNSELECTED = 6, SELECTED};
enum {F=10, B, U, D, R, L};
enum {BLACK=30, WHITE, BLUE, GREEN, RED, PURPLE, YELLOW};


#endif
